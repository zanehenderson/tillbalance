package com.tillbalance;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main {

    // This amount reflects the desired starting amount of a till
    final private static BigDecimal tillFloat = new BigDecimal("400.00");

    // Contains the values of all currencies, in dollar.cents format
    final private static BigDecimal[] currencyValues = {
            new BigDecimal("0.05"),
            new BigDecimal("0.10"),
            new BigDecimal("0.20"),
            new BigDecimal("0.50"),
            new BigDecimal("1.00"),
            new BigDecimal("2.00"),
            new BigDecimal("5.00"),
            new BigDecimal("10.00"),
            new BigDecimal("20.00"),
            new BigDecimal("50.00"),
            new BigDecimal("100.00")
    };

    // Contains the names of all currencies
    final private static String[] currencyNames = {
            "Five cents",
            "Ten cents",
            "Twenty cents",
            "Fifty cents",
            "One dollar",
            "Two dollars",
            "Five dollars",
            "Ten dollars",
            "Twenty dollars",
            "Fifty dollars",
            "One hundred dollars"
    };

    public static void main(String[] args) {
        //Input scanner
        Scanner input = new Scanner(System.in);

        // Loop while still tills to process
        boolean running = true;
        while (running) {
            int tillNo;
            String tillUser;
            int[] tillCounts = new int[11];
            BigDecimal[] tillValues = new BigDecimal[11];
            BigDecimal tillCash = BigDecimal.ZERO;
            BigDecimal tillBanking;
            BigDecimal tillRingoffTotal;
            BigDecimal tillRingoffEftpos;
            BigDecimal tillRingoffCharge;
            BigDecimal tillRingoffVoucher;
            BigDecimal tillActualTotal;
            BigDecimal tillDifference;

            // Get basic till information
            System.out.print("Till number: ");
            tillNo = input.nextInt();
            input.nextLine();
            System.out.print("Till login: ");
            tillUser = input.nextLine();
            System.out.print("Ringoff EFTPOS: ");
            tillRingoffEftpos = new BigDecimal(input.nextLine());
            System.out.print("Ringoff charges: ");
            tillRingoffCharge = new BigDecimal(input.nextLine());
            System.out.print("Ringoff vouchers: ");
            tillRingoffVoucher = new BigDecimal(input.nextLine());
            System.out.print("Ringoff total: ");
            tillRingoffTotal = new BigDecimal(input.nextLine());

            // Get counts for each currency denomination
            // We will then process what these counts are worth
            System.out.println("Enter the count of each denomination:");
            for (int i = 0; i < 11; i++) {
                System.out.print(currencyNames[i] + ": ");
                tillCounts[i] = input.nextInt();
                tillValues[i] = currencyValues[i].multiply(new BigDecimal(tillCounts[i]));
                tillCash = tillCash.add(tillValues[i]);
                input.nextLine();
            }

            System.out.println("Till cash count is " + tillCash.toString());

            // Process the totals for hidden takings
            tillBanking = tillCash.subtract(tillFloat);
            tillActualTotal = tillBanking.add(tillRingoffCharge.add(tillRingoffEftpos.add(tillRingoffVoucher)));
            // Calculate the difference from the ringoff total
            tillDifference = tillActualTotal.subtract(tillRingoffTotal);

            // Was our till good, up or down, and by how much?
            int tillOutBy = tillDifference.compareTo(BigDecimal.ZERO);
            if (tillOutBy == 0) {
                System.out.println("Till " + tillNo + " is good.");
            } else if (tillOutBy < 0) {
                System.out.println("Till " + tillNo + " is DOWN by " + tillDifference.abs().toString());
            } else {
                System.out.println("Till " + tillNo + " is UP by " + tillDifference.abs().toString());
            }

            // The user will need to remove some currency from the till
            System.out.println("Please remove " + tillBanking.toString() + " from the till.");

            // Check if our program needs to continue
            running = false;
            System.out.println("Process another till? (y/n)");
            String answer = input.nextLine();
            if (answer.equals("y")) {
                running = true;
            }
        }
    }
}
